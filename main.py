import matplotlib.pyplot as plt
import socket
import numpy as np

host = "84.237.21.36"
port = 5152
packet_size = 40002


def find_maximums(img):
    maximums = []
    for i in range(1, len(img) - 1):
        for j in range(1, len(img[i] - 1)):
            if img[i - 1][j] < img[i][j] and img[i + 1][j] < img[i][j] and img[i][j - 1] < img[i][j] and img[i][j + 1] < \
                    img[i][j]:
                maximums.append((i, j))
    return maximums


def get_distance(c1, c2):
    return ((c1[0] - c2[0]) ** 2 + (c1[1] - c2[1]) ** 2) ** 0.5


def recvall(sock, n):
    data = bytearray()
    while len(data) < n:
        packet = sock.recv(n - len(data))
        # print(len(data))
        if not packet:
            return
        data.extend(packet)
    # print("DOWNLOADED!")
    return data


plt.ion()
plt.figure()
yeps = 0

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
    sock.connect((host, port))
    beat = b"nope"
    while yeps < 10:
        sock.send(b"get")
        bts = recvall(sock, packet_size)
        rows, cols = bts[:2]
        im1 = np.frombuffer(bts[2:rows * cols + 2], dtype="uint8").reshape(rows, cols)

        pos1 = np.unravel_index(np.argmax(im1), im1.shape)
        plt.clf()

        coords = find_maximums(im1)
        distance = 0
        if len(coords) >= 2:
            distance = round(get_distance(coords[0], coords[1]), 1)
        plt.imshow(im1)
        plt.pause(1)
        print(distance)
        sock.send(str(distance).encode())
        #sock.send(b"beat")
        beat = sock.recv(20)
        print(beat)
        if beat == b'yep':
            yeps += 1

print("Done")
